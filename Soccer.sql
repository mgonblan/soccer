CREATE TABLE `EQUIPO`  (
  `IDEquipo` int(9) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de equipo',
  `Nombre` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del equipo',
  `Fecha_creacion` date NOT NULL COMMENT 'Fecha_creación',
  `Direccion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Dirección del equipo',
  `Web` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Web del equipo',
  `Escudo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Escudo',
  PRIMARY KEY (`IDEquipo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `EVENTOS_PARTIDO`  (
  `Timestamp_evento` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp del evento',
  `Idevento` int(3) NOT NULL COMMENT 'Identificador del evento',
  `IDJugador` int(9) NOT NULL COMMENT 'Jugador que ha realizado el evento',
  `IDPartido` int(9) NOT NULL COMMENT 'Identificador del partido',
  INDEX `fk_partido_evento`(`Idevento`) USING BTREE,
  INDEX `fk_partido`(`IDPartido`, `IDJugador`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `Estadio`  (
  `IDEquipo` int(9) NOT NULL COMMENT 'Identificador de equipo',
  `IDEstadio` int(9) NOT NULL COMMENT 'Identificador de estadio',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del estadio',
  `Direccion_estadio` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Dirección del estadio',
  PRIMARY KEY (`IDEstadio`) USING BTREE,
  INDEX `REL_EQUIPO`(`IDEquipo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `Evento`  (
  `id_evento` int(3) NOT NULL AUTO_INCREMENT COMMENT 'Descripción de evento',
  `Descripción` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Puntaje` int(1) NOT NULL,
  PRIMARY KEY (`id_evento`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `JUGADOR`  (
  `IDJugador` int(9) NOT NULL COMMENT 'Identificador de Jugador',
  `Nombre` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre de Jugador',
  `Apellidos` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Apellidos de jugador',
  `Fecha_nacimiento` date NOT NULL COMMENT 'Fecha de nacimiento de jugador',
  `Apodo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Apodo Jugador',
  `Clausula_Rescision` decimal(18, 2) NOT NULL DEFAULT 0.00 COMMENT 'Clausula Rescision',
  `IDPosicion` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Identificación de posición',
  PRIMARY KEY (`IDJugador`) USING BTREE,
  INDEX `POSICION`(`IDPosicion`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `LIGA`  (
  `AÑO` year NOT NULL COMMENT 'Año de Liga',
  `IdLiga` int(9) NOT NULL COMMENT 'Identificación de la liga',
  `NombreLiga` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre de la liga',
  `idpais` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'País de la liga',
  PRIMARY KEY (`IdLiga`) USING BTREE,
  INDEX `PAISLIGA`(`idpais`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `Mercado`  (
  `TimeStampPuja` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp de la puja',
  `CantidadPuja` decimal(9, 2) NOT NULL COMMENT 'Cantidad de la puja',
  `IDUsuario` int(9) NOT NULL COMMENT 'ID Del usuario',
  `IDJugador` int(9) NOT NULL COMMENT 'ID Del jugador',
  PRIMARY KEY (`IDUsuario`, `IDJugador`, `TimeStampPuja`) USING BTREE,
  INDEX `fk_Mercado_JUGADOR_1`(`IDJugador`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `Pais`  (
  `IdPais` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NombPais` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`IdPais`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `Partido`  (
  `IDEquipoLocal` int(9) NOT NULL COMMENT 'Identificador de equipo Local',
  `IDEquipoVisitante` int(9) NOT NULL COMMENT 'Identificador de equipo visitante',
  `IDJugador` int(9) NOT NULL COMMENT 'Identificador de jugador',
  `IDLiga` int(9) NOT NULL COMMENT 'Identificación de liga',
  `Resumen` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Resumen del partido',
  `IDPartido` int(9) NOT NULL COMMENT 'Identificador de partido',
  `Fechacelebracion` date NOT NULL COMMENT 'Fecha de celebracion',
  PRIMARY KEY (`IDPartido`) USING BTREE,
  UNIQUE INDEX `Secoundary`(`IDEquipoLocal`, `IDEquipoVisitante`, `Fechacelebracion`) USING BTREE COMMENT 'Para conseguir los equipos ',
  INDEX `fk_visitante`(`IDEquipoVisitante`) USING BTREE,
  INDEX `fk_Jugador`(`IDJugador`) USING BTREE,
  INDEX `fk_liga`(`IDLiga`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `Plantilla`  (
  `FechaDesde` datetime(0) NOT NULL COMMENT 'Fecha desde que el jugador participa en el equipo',
  `FechaHasta` datetime(0) NOT NULL COMMENT 'Fecha hasta que el jugador pertenece al equipo',
  `CantidadEquipo` decimal(9, 2) NOT NULL COMMENT 'Cantidad por la que se compró el jugador',
  `IDJugador` int(9) NOT NULL COMMENT 'ID de jugador',
  `IDEquipo` int(9) NOT NULL COMMENT 'ID de Equipo',
  PRIMARY KEY (`IDJugador`, `IDEquipo`, `FechaDesde`, `FechaHasta`) USING BTREE,
  INDEX `PLANTILLA_EQUIPO`(`IDEquipo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `Posición`  (
  `IDPosicion` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Descripción de la posición',
  `NombrePosicion` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DescripcionPosicion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`IDPosicion`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `USUARIO`  (
  `IDUsuario` int(9) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de usuario',
  `Nombre` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre de usuario',
  `Apellidos` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Apellidos de Usuario',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'email de usuario',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Password de usuario',
  `Timestamp_alta` datetime(0) NOT NULL COMMENT 'Timestamp de alta de usuario',
  `Apodo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Apodo del usuario',
  PRIMARY KEY (`IDUsuario`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

ALTER TABLE `EVENTOS_PARTIDO` ADD CONSTRAINT `fk_partido` FOREIGN KEY (`IDPartido`, `IDJugador`) REFERENCES `Partido` (`IDJugador`, `IDPartido`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `EVENTOS_PARTIDO` ADD CONSTRAINT `fk_partido_evento` FOREIGN KEY (`Idevento`) REFERENCES `Evento` (`id_evento`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Estadio` ADD CONSTRAINT `REL_EQUIPO` FOREIGN KEY (`IDEquipo`) REFERENCES `EQUIPO` (`IDEquipo`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `JUGADOR` ADD CONSTRAINT `POSICION` FOREIGN KEY (`IDPosicion`) REFERENCES `Posición` (`IDPosicion`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `LIGA` ADD CONSTRAINT `PAISLIGA` FOREIGN KEY (`idpais`) REFERENCES `Pais` (`IdPais`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `Mercado` ADD CONSTRAINT `FK_MERCADO_USUARIO` FOREIGN KEY (`IDUsuario`) REFERENCES `USUARIO` (`IDUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Mercado` ADD CONSTRAINT `fk_Mercado_JUGADOR_1` FOREIGN KEY (`IDJugador`) REFERENCES `JUGADOR` (`IDJugador`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Partido` ADD CONSTRAINT `fk_Jugador` FOREIGN KEY (`IDJugador`) REFERENCES `Plantilla` (`IDJugador`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `Partido` ADD CONSTRAINT `fk_liga` FOREIGN KEY (`IDLiga`) REFERENCES `LIGA` (`IdLiga`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `Partido` ADD CONSTRAINT `fk_local` FOREIGN KEY (`IDEquipoLocal`) REFERENCES `EQUIPO` (`IDEquipo`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `Partido` ADD CONSTRAINT `fk_visitante` FOREIGN KEY (`IDEquipoVisitante`) REFERENCES `EQUIPO` (`IDEquipo`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `Plantilla` ADD CONSTRAINT `PLANTILLA_EQUIPO` FOREIGN KEY (`IDEquipo`) REFERENCES `EQUIPO` (`IDEquipo`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Plantilla` ADD CONSTRAINT `fk_Plantilla_JUGADOR_1` FOREIGN KEY (`IDJugador`) REFERENCES `JUGADOR` (`IDJugador`);

